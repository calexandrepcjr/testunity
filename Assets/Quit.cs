﻿using System.Collections;
using System.Collections.Generic;
#if (UNITY_EDITOR)
using UnityEditor;
#endif
using UnityEngine;

public class Quit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void QuitApp()
    {
        #if (UNITY_EDITOR)
            EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
}
