﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Foo {
    private static string[] scenes = new string[] { "Assets/testproject.unity" };
	
    public static void PerformWinBuild()
    {
        BuildPipeline.BuildPlayer(scenes, "Build/Win/test.exe", BuildTarget.StandaloneWindows, BuildOptions.None);
    }

    public static void PerformAndroidBuild()
    {
        BuildPipeline.BuildPlayer(scenes, "Build/Android/test.apk", BuildTarget.Android, BuildOptions.None);
    }
}
